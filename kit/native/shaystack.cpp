/** 
 * This file contains Sedona native C binding functions.
 */

#include "sedona.h"
#include "haystack/grid.hpp"
#include "http_client/include/client.hpp"
#include "haystack/auth/clientcontext.hpp"
#include "http_client/include/HTTPResponseStringParser.hpp"
#include "zincwriter.hpp"
#include "zincreader.hpp"
#include "num.hpp"
#include "bool.hpp"
#include <iostream>


extern "C" Cell shaystack_HaystackDevice_create_client(SedonaVM* vm, Cell* params) {
	const char* uri = static_cast<const char*> (params[0].aval);
	const char* username = static_cast<const char*> (params[1].aval);
	const char* password = static_cast<const char*> (params[2].aval);

	Cell result;
	try {
		haystack::Client* client = new haystack::Client( uri, username, password );
		result.aval = client;
		return result;
	} catch ( const std::exception& e ) {
		std::cerr << "EXCEPTION: " << e.what( ) << std::endl;
		return nullCell;
	}
}

extern "C" Cell shaystack_HaystackDevice_delete_client(SedonaVM* vm, Cell* params) {
	try {
	haystack::Client* client = static_cast<haystack::Client*> (params[0].aval);
	delete client;
	} catch ( const std::exception& e ) {
		std::cerr << "EXCEPTION: " <<__FILE__<<":"<<__LINE__<<":"<< e.what( ) << std::endl;
		return nullCell;
	}
}

extern "C" Cell shaystack_HaystackDevice_is_authenticated(SedonaVM* vm, Cell* params) {
	try {
	haystack::Client* client = static_cast<haystack::Client*> (params[0].aval);
	return client->is_authenticated() ? trueCell : falseCell;
	} catch ( const std::exception& e ) {
		std::cerr << "EXCEPTION: " <<__FILE__<<":"<<__LINE__<<":"<< e.what( ) << std::endl;
		return nullCell;
	}
}

extern "C" Cell shaystack_HaystackDevice_isSessionConnected(SedonaVM* vm, Cell* params) {
	try {
	haystack::Client* client = static_cast<haystack::Client*> (params[0].aval);
	return client->isSessionConnected() ? trueCell : falseCell;
	} catch ( const std::exception& e ) {
		std::cerr << "EXCEPTION: " <<__FILE__<<":"<<__LINE__<<":"<< e.what( ) << std::endl;
		return nullCell;
	}
}
/**
 *
 * @param vm
 * @param params
 *	Client* client
 *	last response string
 *  buffer for next request
 *  buffer len
 * @return true or false if nothing to send
 */
extern "C" Cell shaystack_HaystackDevice_get_auth_message(SedonaVM* vm, Cell* params) {
	try {
	haystack::Client* client = static_cast<haystack::Client*> (params[0].aval);
	const char* last_response = static_cast<const char*> (params[1].aval);
	char* buf = static_cast<char*> (params[2].aval);
	int buf_len = params[3].ival;
	HTTPRequest req;
	if ( client->processAuthMessage(req, last_response) ) {
		std::stringstream ss;
		req.write(ss);
		if ( ss.str().empty() ) {
			return falseCell;
		}

		size_t length = ss.str().copy(buf, buf_len);
		buf[length] = '\0';
		return trueCell;

	} else {
		return falseCell; // complete auth
	}
	} catch ( const std::exception& e ) {
		std::cerr << "EXCEPTION: " <<__FILE__<<":"<<__LINE__<<":"<< e.what( ) << std::endl;
		return nullCell;
	}
}

extern "C" Cell shaystack_HaystackDevice_read_message(SedonaVM* vm, Cell* params) {
	try {
	haystack::Client* client = static_cast<haystack::Client*> (params[0].aval);
	const char* filter = static_cast<const char*> (params[1].aval);
	char* buf = static_cast<char*> (params[2].aval);
	int buf_len = params[3].ival;
	const std::string message = client->get_read_all_message(filter, 1);
	message.copy(buf, buf_len);
	buf[message.length()] = '\0';
	return trueCell;
	} catch ( const std::exception& e ) {
		std::cerr << "EXCEPTION: " <<__FILE__<<":"<<__LINE__<<":"<< e.what( ) << std::endl;
		return nullCell;
	}
}

extern "C" Cell shaystack_HaystackDevice_eval_message(SedonaVM* vm, Cell* params) {
	try {
	haystack::Client* client = static_cast<haystack::Client*> (params[0].aval);
	const char* filter = static_cast<const char*> (params[1].aval);
	char* buf = static_cast<char*> (params[2].aval);
	int buf_len = params[3].ival;
	const std::string message = client->get_eval_message(filter);
	message.copy(buf, buf_len);
	buf[message.length()] = '\0';
	return trueCell;
	} catch ( const std::exception& e ) {
		std::cerr << "EXCEPTION: " <<__FILE__<<":"<<__LINE__<<":"<< e.what( ) << std::endl;
		return nullCell;
	}
}

extern "C" Cell shaystack_HaystackDevice_watch_sub_message(SedonaVM* vm, Cell* params) {
	haystack::Client* client = static_cast<haystack::Client*> (params[0].aval);
	const char* dis = static_cast<const char*> (params[1].aval);
	const char* watch_id = static_cast<const char*> (params[2].aval);
	const char* ref = static_cast<const char*> (params[3].aval);

	char* buf = static_cast<char*> (params[4].aval);
	int buf_len = params[5].ival;

	try {
		const std::string message = client->get_watch_sub(dis, watch_id, { ref } );
		message.copy(buf, buf_len);
		buf[message.length()] = '\0';
		return trueCell;
	} catch (std::runtime_error& e) {
		std::cerr << "exception: " << e.what() << endl;
		return falseCell;
	}
}

extern "C" Cell shaystack_HaystackDevice_watch_unsub_message(SedonaVM* vm, Cell* params) {
	try {
	haystack::Client* client = static_cast<haystack::Client*> (params[0].aval);
	const char* watch_id = static_cast<const char*> (params[1].aval);
	const char* ref = static_cast<const char*> (params[2].aval);

	char* buf = static_cast<char*> (params[3].aval);
	int buf_len = params[4].ival;

	const std::string message = client->get_watch_unsub(watch_id, { ref } );
	message.copy(buf, buf_len);
	buf[message.length()] = '\0';
	return trueCell;
	} catch ( const std::exception& e ) {
		std::cerr << "EXCEPTION: " <<__FILE__<<":"<<__LINE__<<":"<< e.what( ) << std::endl;
		return nullCell;
	}
}

extern "C" Cell shaystack_HaystackDevice_watch_poll_message(SedonaVM* vm, Cell* params) {
	try {
	haystack::Client* client = static_cast<haystack::Client*> (params[0].aval);
	const char* watch_id = static_cast<const char*> (params[1].aval);

	char* buf = static_cast<char*> (params[2].aval);
	int buf_len = params[3].ival;

	const std::string message = client->get_watch_poll( watch_id );
	message.copy(buf, buf_len);
	buf[message.length()] = '\0';
	return trueCell;
	} catch ( const std::exception& e ) {
		std::cerr << "EXCEPTION: " <<__FILE__<<":"<<__LINE__<<":"<< e.what( ) << std::endl;
		return nullCell;
	}
}

haystack::Grid::auto_ptr_t parse_response(const std::string& res) {
	try {
	HTTPResponse response;
	std::stringstream ss(res);
	HTTPResponseStringParser parser( ss );
	std::string body = parser.parse(response);

	if (response.getStatus() >= HTTPResponse::HTTP_BAD_REQUEST) {
		throw std::runtime_error(response.getReason());
	}

	std::stringstream bodystream(body);
	haystack::ZincReader reader(bodystream);
	return reader.read_grid();
	} catch ( const std::exception& e ) {
		std::cerr << "EXCEPTION: " <<__FILE__<<":"<<__LINE__<<":"<< e.what( ) << std::endl;
	}
	return haystack::Grid::auto_ptr_t(new haystack::Grid());
}

extern "C" Cell shaystack_HaystackDevice_has_float(SedonaVM* vm, Cell* params) {
	const char* response = static_cast<const char*> (params[0].aval);
	int rownum = params[1].ival;
	const char* colName = static_cast<const char*> (params[2].aval);

	try {
	    haystack::Grid::auto_ptr_t grid = parse_response(response);
	    if ( grid->is_empty() || grid->is_err() || grid->num_rows() <= rownum)
		    return falseCell;

		const haystack::Val& val = grid->row(rownum).get(colName);
		return val.type() == haystack::Val::NUM_TYPE ? trueCell : falseCell;

	} catch (std::runtime_error& e) {
		std::cerr << "EXCEPTION in has_float: " << e.what() << endl;
		return falseCell;
	}
}

extern "C" Cell shaystack_HaystackDevice_has_bool(SedonaVM* vm, Cell* params) {
	const char* response = static_cast<const char*> (params[0].aval);
	int rownum = params[1].ival;
	const char* colName = static_cast<const char*> (params[2].aval);

	try {
	    haystack::Grid::auto_ptr_t grid = parse_response(response);
	    if ( grid->is_empty() || grid->is_err() || grid->num_rows() <= rownum)
		    return falseCell;

		const haystack::Val& val = grid->row(rownum).get(colName);
		return val.type() == haystack::Val::BOOL_TYPE ? trueCell : falseCell;
	} catch (std::runtime_error& e) {
		std::cerr << "EXCEPTION in has_bool: " << e.what() << endl;
		return falseCell;
	}
}

extern "C" Cell shaystack_HaystackDevice_parse_float_response(SedonaVM* vm, Cell* params) {
	const char* response = static_cast<const char*> (params[0].aval);
	int rownum = params[1].ival;
	const char* colName = static_cast<const char*> (params[2].aval);
	haystack::Client* client = static_cast<haystack::Client*> (params[3].aval);
    try {
	    haystack::Grid::auto_ptr_t grid = parse_response(response);
	    if ( grid->is_empty() || grid->is_err() || grid->num_rows() <= rownum)
		    return nullCell;

		Cell ret;
		ret.fval = static_cast<float>(grid->row(rownum).get_double(colName));
		return ret;
	} catch (std::runtime_error& e) {
		std::cerr << "EXCEPTION in parse_float_response: " << e.what() << endl;
		client->clearAuth();
		return nullCell;
	}
}

extern "C" Cell shaystack_HaystackDevice_parse_bool_response(SedonaVM* vm, Cell* params) {
	const char* response = static_cast<const char*> (params[0].aval);
	int rownum = params[1].ival;
	const char* colName = static_cast<const char*> (params[2].aval);

	try {
	    haystack::Grid::auto_ptr_t grid = parse_response(response);
	    if ( grid->is_empty() || grid->is_err() || grid->num_rows() <= rownum)
		    return nullCell;

		Cell ret;
		ret.ival = static_cast<bool>(grid->row(rownum).get_bool(colName));
		return ret;
	} catch (std::runtime_error& e) {
		std::cerr << "EXCEPTION in parse_bool_response: " << e.what() << endl;
		return nullCell;
	}
}


extern "C" Cell shaystack_HaystackDevice_parse_str_response(SedonaVM* vm, Cell* params) {

    const char* response = static_cast<const char*> (params[0].aval);
    int rownum = params[1].ival;
    const char* colName = static_cast<const char*> (params[2].aval);
    bool checked = params[3].ival;

    char* buf = static_cast<char*> (params[4].aval);
    int buf_len = params[5].ival;

    try {
		haystack::Grid::auto_ptr_t grid = parse_response(response);
		if ( grid->is_err() ) {
			std::cerr << grid->meta().get_str("dis") << endl;
		}

		if ( grid->is_empty() || grid->is_err() || (rownum >= 0 && grid->num_rows() <= rownum))
			return falseCell;

		std::string val;
		if ( rownum >= 0 ) {
			if ( checked ) {
				val = grid->row(rownum).get_str(colName);
			} else {
				val = grid->row(rownum).get(colName).to_string();
			}
		} else { // meta
			if ( checked ) {
				val = grid->meta().get_str(colName);
			} else {
				val = grid->meta().get(colName).to_string();
			}
		}
		val.copy(buf, buf_len);
		buf[val.length()] = '\0';
		return trueCell;
	} catch (std::runtime_error& e) {
		std::cerr << "EXCEPTION in parse_str_response: " << e.what() << endl;
		return falseCell;
	}
}


extern "C" Cell shaystack_HaystackDevice_is_empty(SedonaVM* vm, Cell* params) {
	const char* response = static_cast<const char*> (params[0].aval);

	try {
	    haystack::Grid::auto_ptr_t grid = parse_response(response);
	    Cell ret;
	    ret.ival = grid->is_empty();
	    return ret;

	} catch(std::runtime_error& e) {
        std::cerr << "EXCEPTION in HaystackDevice_is_empty: " << e.what() << endl;
        return nullCell;
    }
}

extern "C" Cell shaystack_HaystackDevice_is_err(SedonaVM* vm, Cell* params) {
	const char* response = static_cast<const char*> (params[0].aval);

	try {
	    haystack::Grid::auto_ptr_t grid = parse_response(response);
	    Cell ret;
	    ret.ival = grid->is_err();
        return ret;

    } catch(std::runtime_error& e) {
        std::cerr << "EXCEPTION in HaystackDevice_is_err: " << e.what() << endl;
        return nullCell;
    }
}

extern "C" Cell shaystack_HaystackDevice_write_float_point_message(SedonaVM* vm, Cell* params) {
	try {
	haystack::Client* client = static_cast<haystack::Client*> (params[0].aval);
	const char* point_id = static_cast<const char*> (params[1].aval);
	int level = params[2].ival;
	float val = params[3].fval;
	int duration = params[4].ival;

	char* buf = static_cast<char*> (params[5].aval);
	int buf_len = params[6].ival;

	std::string message = client->get_point_write_float(point_id, level, val, duration);
	message.copy(buf, buf_len);
	buf[message.length()] = '\0';
	} catch ( const std::exception& e ) {
		std::cerr << "EXCEPTION: " <<__FILE__<<":"<<__LINE__<<":"<< e.what( ) << std::endl;
	}
	return trueCell;
}


extern "C" Cell shaystack_HaystackDevice_write_bool_point_message(SedonaVM* vm, Cell* params) {
	try {
	haystack::Client* client = static_cast<haystack::Client*> (params[0].aval);
	const char* point_id = static_cast<const char*> (params[1].aval);
	int level = params[2].ival;
	bool val = params[3].ival;
	int duration = params[4].ival;

	char* buf = static_cast<char*> (params[5].aval);
	int buf_len = params[6].ival;

	std::string message = client->get_point_write_bool(point_id, level, val, duration);
	message.copy(buf, buf_len);
	buf[message.length()] = '\0';
	} catch ( const std::exception& e ) {
		std::cerr << "EXCEPTION: " <<__FILE__<<":"<<__LINE__<<":"<< e.what( ) << std::endl;
	}
	return trueCell;
}

extern "C" Cell shaystack_HaystackDevice_reset_point_message(SedonaVM* vm, Cell* params) {
	try {
	haystack::Client* client = static_cast<haystack::Client*> (params[0].aval);
	const char* point_id = static_cast<const char*> (params[1].aval);
	int level = params[2].ival;

	char* buf = static_cast<char*> (params[3].aval);
	int buf_len = params[4].ival;

	std::string message = client->get_point_write_reset(point_id, level);
	message.copy(buf, buf_len);
	buf[message.length()] = '\0';
	} catch ( const std::exception& e ) {
		std::cerr << "EXCEPTION: " <<__FILE__<<":"<<__LINE__<<":"<< e.what( ) << std::endl;
	}
	return trueCell;
}

extern "C" Cell shaystack_HaystackDevice_write_read_point_message(SedonaVM* vm, Cell* params) {
	try {
	haystack::Client* client = static_cast<haystack::Client*> (params[0].aval);
	const char* point_id = static_cast<const char*> (params[1].aval);

	char* buf = static_cast<char*> (params[2].aval);
	int buf_len = params[3].ival;

	std::string message = client->get_point_write_read(point_id);
	message.copy(buf, buf_len);
	buf[message.length()] = '\0';
	} catch ( const std::exception& e ) {
		std::cerr << "EXCEPTION: " <<__FILE__<<":"<<__LINE__<<":"<< e.what( ) << std::endl;
	}
	return trueCell;
}

/// @param uri
/// @param username
/// @param password
extern "C" Cell shaystack_HaystackDevice_open(SedonaVM* vm, Cell* params) {
	const char* uri = static_cast<const char*> (params[0].aval);
	const char* username = static_cast<const char*> (params[1].aval);
	const char* password = static_cast<const char*> (params[2].aval);

	Cell result;
	try {
		haystack::Client* client = new haystack::Client( uri, username, password );
		client->open();
		result.aval = client; //haystack::Client::open(uri, username, password).get();
//		cout << "open: " << client << endl;

	} catch ( const std::exception& e ) {
		std::cerr << "EXCEPTION: " << e.what( ) << std::endl;
		result.aval = NULL;
	}

	return result;
}

extern "C" Cell shaystack_HaystackDevice_call(SedonaVM* vm, Cell* params) {
	haystack::Client* client = static_cast<haystack::Client*> (params[0].aval);
	const char* op = static_cast<const char*> (params[1].aval);
//	cout << client << " " << op << endl;

	auto grid = client->call(op, haystack::Grid::EMPTY);
	try {
		cout << haystack::ZincWriter::grid_to_string(*grid) << endl;
	} catch (...) {
		cout << grid->row(0).to_dict()->to_string() << endl;;
	}
}

/// @param client context
/// @param op
extern "C" Cell shaystack_HaystackDevice_read_by_id(SedonaVM* vm, Cell* params) {
	
	//client->read_by_id();
}

/// @param client context
/// @param filter
/// @param limit
extern "C" Cell shaystack_HaystackDevice_read_float(SedonaVM* vm, Cell* params) {
	haystack::Client* client = static_cast<haystack::Client*> (params[0].aval);
	const char* filter = static_cast<const char*> (params[1].aval);
	Cell cell;
	
	try {
		//cout << client << " " << filter << " limit: " << limit << endl;
		haystack::Dict::auto_ptr_t res = client->read(filter);
		//haystack::ZincWriter w(cout);
		//w.write_grid(*res);

		// TODO grid.isErr, grid.isEmpty handle
		if ( ! res->is_empty() ) {
			const haystack::Val& result = res->get("curVal");
	//		cout << "result type: " << (char)result.type() << endl;

			if ( result.type() == haystack::Val::NUM_TYPE ) {
				cell.fval = result.as<haystack::Num>().value;
			} else {
				//cell.ival = result.as<haystack::Bool>().value ? 1 : 0;
				throw std::runtime_error(std::string("Expection Num, got ") + (char)result.type() + " for filter: " + filter );
			}
		}
	}
	catch ( const std::exception& e ) {
		std::cerr << "EXCEPTION: " << e.what( ) << std::endl;
		return nullCell;
	}
	
	return cell;
}

/// @param client context
/// @param filter
/// @param limit
extern "C" Cell shaystack_HaystackDevice_read_bool(SedonaVM* vm, Cell* params) {
	haystack::Client* client = static_cast<haystack::Client*> (params[0].aval);
	const char* filter = static_cast<const char*> (params[1].aval);
	Cell cell;
	haystack::Dict::auto_ptr_t res;
	try {
		//cout << client << " " << filter << " limit: " << limit << endl;
		res = client->read(filter);
//		haystack::ZincWriter w(cout);
//		w.write_grid(*res);

		// TODO grid.isErr, grid.isEmpty handle
		if ( ! res->is_empty() ) {
			const haystack::Val& result = res->get("curVal");
	//		cout << "result type: " << (char)result.type() << endl;

			if ( result.type() == haystack::Val::BOOL_TYPE ) {
				cell.ival = result.as<haystack::Bool>().value;
			} else {
				//cell.ival = result.as<haystack::Bool>().value ? 1 : 0;
				throw std::runtime_error(std::string("Expection Bool, got ") + (char)result.type() + " for filter: " + filter );
			}
		}
	}
	catch ( const std::exception& e ) {
		std::cerr << "EXCEPTION: " << e.what( ) << std::endl;
		return nullCell;
	}

	return cell;
}

/// @param client context
/// @param filter
extern "C" Cell shaystack_HaystackDevice_eval_bool(SedonaVM* vm, Cell* params) {
	haystack::Client* client = static_cast<haystack::Client*> (params[0].aval);
	const char* expr = static_cast<const char*> (params[1].aval);
	int rowNum = params[2].ival;
	const char* colName = static_cast<const char*> (params[3].aval);

	// arguments of this code block:
	//
	// type of result  = haystack::Val::BOOL_TYPE
	//

	Cell cell;

	try {
		haystack::Grid::auto_ptr_t res  = client->eval(expr);
		if ( res->is_err() ) {
			std::cerr << "Error for '" << expr << "': " << res->row(0).get("err").to_string() << endl;
			return nullCell;
		}

		if ( res->num_rows() <= rowNum ) {
			std::cerr << "Too few rows in result of '" << expr << "': ";
			haystack::ZincWriter w(std::cerr);
			w.write_grid(*res);
			return nullCell;
		}

		const haystack::Val& result = res->row(rowNum).get(colName);
		if ( result.type() != haystack::Val::BOOL_TYPE ) {
			std::cerr << "Wrong type of cell in row #" << rowNum << " column: " << colName << " result of '" << expr << "': ";
			haystack::ZincWriter w(std::cerr);
			w.write_grid(*res);
			return nullCell;
		}

		cell.ival = result.as<haystack::Bool>().value;
		return cell;
		
	}
	catch ( const std::exception& e ) {
		std::cerr << "EXCEPTION: " << e.what( ) << std::endl;
		return nullCell;
	}
	
}


/// @param client context
/// @param filter
extern "C" Cell shaystack_HaystackDevice_eval_float(SedonaVM* vm, Cell* params) {
	haystack::Client* client = static_cast<haystack::Client*> (params[0].aval);
	const char* expr = static_cast<const char*> (params[1].aval);
	int rowNum = params[2].ival;
	const char* colName = static_cast<const char*> (params[3].aval);

	// arguments of this code block:
	//
	// type of result  = haystack::Val::BOOL_TYPE
	//

	Cell cell;

	try {
		haystack::Grid::auto_ptr_t res  = client->eval(expr);
		if ( res->is_err() ) {
			std::cerr << "Error for '" << expr << "': " << res->row(0).get("err").to_string() << endl;
			return nullCell;
		}

		if ( res->num_rows() <= rowNum ) {
			std::cerr << "Too few rows in result of '" << expr << "': ";
			haystack::ZincWriter w(std::cerr);
			w.write_grid(*res);
			return nullCell;
		}

//		haystack::ZincWriter w(std::cerr);
//		w.write_grid(*res);

		const haystack::Val& result = res->row(rowNum).get(colName);
//		std::cout << "result: " << result.to_string() << endl;
		if ( result.type() != haystack::Val::NUM_TYPE ) {
			std::cerr << "Wrong type of cell in row #" << rowNum << " column: " << colName << " result of '" << expr << "': ";
			haystack::ZincWriter w(std::cerr);
			w.write_grid(*res);
			return nullCell;
		}

		cell.fval = result.as<haystack::Num>().value;
		return cell;

	}
	catch ( const std::exception& e ) {
		std::cerr << "EXCEPTION: " << e.what( ) << std::endl;
		return nullCell;
	}

}

extern "C" Cell shaystack_HaystackDevice_is_filter_valid(SedonaVM* vm, Cell* params) {
	const char* expr = static_cast<const char*> (params[0].aval);
	Cell res;
	try {
		haystack::ZincReader::make(expr)->read_filter();
		res.ival = true;
	} catch(...) {
		res.ival = false;
	}

	return res;
}

//void sedonaHaystackOpen(const char* uri, const char* username, const char* password);

