/* 
 * File:   DummyHTTPClientSession.hpp
 * Author: Andrey Zakharov <aazaharov81@gmail.com>
 *
 * Created on 16 октября 2018 г., 7:54
 */

#pragma once
#include <Poco/Net/HTTPResponse.h>
using namespace Poco::Net;
/**
 * We need this class to use instead HTTPClientSession
 * to get all socket data from our outer string
 * Think about it as in-memory socket data based session
 */
class HTTPResponseStringParser {
public:
    HTTPResponseStringParser( std::istream& data_ ) : _data(data_) {}
    /**
     * Puts header into req, and returns body as string
     *
     * @param req
     * @return
     */
    std::string parse( HTTPResponse& response );


private:
    std::string getFixedLength(std::streamsize len);
    std::string getChunked();
    std::istream& _data;

};
