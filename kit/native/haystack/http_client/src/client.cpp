//
// Copyright (c) 2015, J2 Innovations
// Copyright (c) 2012 Brian Frank
// Licensed under the Academic Free License version 3.0
// History:
//   19 Sep 2014  Radu Racariu<radur@2inn.com> Ported to C++
//   06 Jun 2011  Brian Frank  Creation
//

#include "client.hpp"

#include <string>
#include <sstream>
#include <iostream>
#include <stdexcept>

#include <boost/algorithm/string.hpp>

#include <Poco/Base64Encoder.h>
#include <Poco/Exception.h>
#include <Poco/Net/HTTPClientSession.h>
#include <Poco/Net/HTTPRequest.h>
#include <Poco/Net/HTTPResponse.h>
#include <Poco/Path.h>
#include <Poco/URI.h>
#include <Poco/Net/HTTPStream.h>
#include <Poco/Net/HTTPFixedLengthStream.h>
#include <Poco/Net/HTTPChunkedStream.h>

#include "num.hpp"
#include "bool.hpp"
#include "zincreader.hpp"
#include "zincwriter.hpp"
#include "HTTPResponseStringParser.hpp"

using namespace haystack;
using namespace Poco;
using namespace Poco::Net;

Client::Client(const std::string& uri, const std::string& user, const std::string& pass) :
m_uri(boost::ends_with(uri, "/") ? uri : uri + "/"),
m_user(user),
m_pass(pass)
{
    // check uri
    if (!boost::starts_with(uri, "http://") && !boost::starts_with(uri, "https://"))
        throw std::runtime_error("Invalid uri format: " + uri);

    // sanity check arguments
    if (user.empty()) throw std::runtime_error("user cannot be empty string");

    // Initialize session
    m_session.reset(new HTTPClientSession(m_uri.getHost(), m_uri.getPort()));
    session().setKeepAlive(true);
    m_auth.reset(new Auth::AuthClientContext(uri + "about", user, pass));
}

bool Client::isSessionConnected() {
    Dict::auto_ptr_t d = about();
    return ((d.get()!=NULL) && (!d->is_empty())); 
}

Client::auto_ptr_t Client::open(const std::string& uri, const std::string& user, const std::string& pass, const int cTo, const int rTo)
{
    Client::auto_ptr_t c(new Client(uri, user, pass));
	if ( cTo ) { c->connectTimeout = cTo; }
	if ( rTo ) { c->readTimeout = rTo; }
    c->open();
    return c;
}

Client& Client::open()
{
	m_auth->connectTimeout = this->connectTimeout;
	m_auth->readTimeout = this->readTimeout;
    m_auth->open();
    return *this;
}

bool Client::processAuthMessage(HTTPRequest& nextRequest, const std::string& lastResponse) {
	if ( lastResponse.empty() ) {
		m_auth->getHelloMsg(nextRequest);
		return true;
	}
	HTTPResponse response;
	std::stringstream is(lastResponse);
	response.read(is);
	return m_auth->asyncAuthStep(response, nextRequest);
}

Dict::auto_ptr_t Client::about() const
{
    Grid::auto_ptr_t g = call("about", Grid::EMPTY);

    return g->row(0).to_dict();
}

Grid::auto_ptr_t Client::ops()
{
    return call("ops", Grid::EMPTY);
}

Grid::auto_ptr_t Client::formats()
{
    return call("formats", Grid::EMPTY);
}

Grid::auto_ptr_t Client::call(const std::string& op, const Grid& req) const
{
    Grid::auto_ptr_t res = post_grid(op, req);
    if (res->is_err()) throw std::runtime_error("Call error: " + res->meta().get_str("dis"));
    return res;
}

const std::string Client::get_call_string(const std::string& op, const Grid& req) const {
	const std::string& reqStr = ZincWriter::grid_to_string(req);
	return get_post_string(op, reqStr);
}

Grid::auto_ptr_t Client::eval(const std::string& expr) const {
	Grid req;
    req.add_col("expr");
	Val* row[1] = { new Str(expr) };
    req.add_row(row, 1);
    return call("eval", req);
}

// impl

Grid::auto_ptr_t Client::post_grid(const std::string& op, const Grid& req) const
{
    const std::string& reqStr = ZincWriter::grid_to_string(req);
    const std::string& resStr = const_cast<Client&>(*this).post_string(op, reqStr);
    return ZincReader::make(resStr)->read_grid();
}

const std::string Client::post_string(const std::string& op, const std::string& req)
{
    HTTPClientSession session(m_uri.getHost(), m_uri.getPort());
    HTTPRequest post(HTTPRequest::HTTP_POST, m_uri.getPathAndQuery() + op, HTTPMessage::HTTP_1_1);
    post.setKeepAlive(true);

    m_auth->prepare(post);
    post.setContentLength(req.length());
    post.setContentType("text/zinc; charset=utf-8");
    session.sendRequest(post) << req;

    HTTPResponse res;
    std::stringstream ss;
    std::istream& rs = session.receiveResponse(res);
    if (res.getStatus() != HTTPResponse::HTTP_OK && res.getStatus() != HTTPResponse::HTTP_FOUND)
        throw std::runtime_error("POST failed");

    std::istreambuf_iterator<char> eos;
    std::string result(std::istreambuf_iterator<char>(rs), eos);
    return result;
}

const std::string Client::get_post_string(const std::string& op, const std::string& req) const {
	HTTPRequest post(HTTPRequest::HTTP_POST, m_uri.getPathAndQuery() + op, HTTPMessage::HTTP_1_1);
	post.setKeepAlive(true);
	m_auth->prepare(post);
    post.setContentLength(req.length());
    post.setContentType("text/zinc; charset=utf-8");
	std::stringstream ss;
	post.write(ss);
	ss << req;
	return ss.str();
}

// stubs

Dict::auto_ptr_t Client::on_read_by_id(const haystack::Ref &r) const
{
    boost::ptr_vector<Ref> v;
    v.push_back(new_clone(r));
    Grid::auto_ptr_t res = read_by_ids(v, false);
    if (res.get() == NULL || res->is_empty()) return  Dict::auto_ptr_t();
    const Row& rec = res->row(0);
    if (rec.missing("id")) return Dict::auto_ptr_t();
    return rec.to_dict();
}

Grid::auto_ptr_t Client::on_read_by_ids(const boost::ptr_vector<Ref> &r) const
{
    Grid req;
    req.add_col("id");
    for (boost::ptr_vector<Ref>::const_iterator it = r.begin(); it != r.end(); ++it)
    {
        Val* v[1] = { new_clone(*it) };
        req.add_row(v, 1);
    }

    return call("read", req);
}

Grid::auto_ptr_t Client::on_read_all(const std::string & filter, size_t limit) const
{
    Grid req;
    req.add_col("filter");
    req.add_col("limit");
    Val* v[2] = { new Str(filter), new Num((long long)limit) };
    req.add_row(v, 2);
    return call("read", req);
}

const std::string Client::get_read_all_message(const std::string &filter, size_t limit) const {
	Grid req;
    req.add_col("filter");
    req.add_col("limit");
    Val* v[2] = { new Str(filter), new Num((long long)limit) };
    req.add_row(v, 2);
	return get_call_string("read", req);
}

const std::string Client::get_eval_message(const std::string &expr) const {
	Grid req;
    req.add_col("expr");
	Val* row[1] = { new Str(expr) };
    req.add_row(row, 1);
	return get_call_string("eval", req);
}

const std::string Client::get_watch_sub(const std::string& dis, const std::string& id, const std::vector<std::string> refs) {

	Grid req;
	req.meta().add("watchDis", dis);
	if ( !id.empty() ) req.meta().add("watchId", id);

	req.add_col("id");
	for( const auto& ref : refs ) {
		Val* row[1] = { new Ref(ref) };
		req.add_row(row, 1);
	}

	return get_call_string("watchSub", req);

}

const std::string Client::get_watch_unsub(const std::string& id, const std::vector<std::string> refs) {
	Grid req;
	req.meta().add("watchId", id);

    req.add_col("id");
	for( const auto& ref : refs ) {
		Val* row[1] = { new Ref(ref) };
		req.add_row(row, 1);
	}

	return get_call_string("watchUnsub", req);
}

const std::string Client::get_watch_poll(const std::string& id) {
	Grid req;
	req.meta().add("watchId", id);
	req.add_col("empty");
	return get_call_string("watchPoll", req);
}

const std::string Client::get_point_write_float(const std::string& id, int level, float val, int duration) {
	Grid req;
	req.add_col("id");
	req.add_col("level");
	req.add_col("val");
	req.add_col("duration");
	Val* row[4] = { new Ref(id), new Num(level), new Num(val), new Num(duration, "sec") };
	req.add_row(row, 4);

	return get_call_string("pointWrite", req);
}

const std::string Client::get_point_write_bool(const std::string& id, int level, bool val, int duration) {
	Grid req;
	req.add_col("id");
	req.add_col("level");
	req.add_col("val");
	req.add_col("duration");
	Val* row[4] = { new Ref(id), new Num(level), new haystack::Bool(val), new Num(duration, "sec") };
	req.add_row(row, 4);

	return get_call_string("pointWrite", req);
}

const std::string Client::get_point_write_read(const std::string& id) {
	Grid req;
	req.add_col("id");
    req.add_col("level");
    req.add_col("val");
	Val* row[1] = { new Ref(id) };
	req.add_row(row, 1);
	return get_call_string("pointWrite", req);
}

const std::string Client::get_point_write_reset(const std::string& id, int level) {
    Grid req;
    req.add_col("id");
    req.add_col("level");
    req.add_col("val");
    Val* row[3] = { new Ref(id), new Num(level), haystack::new_clone(haystack::EmptyVal::DEF) };
    req.add_row(row, 3);
    return get_call_string("pointWrite", req);
}

/*
int main(int argc, char*argv[])
{
    try
    {
        Client c("http://localhost:8085/auth/", "demo", "demo");

        std::cout << c.open().about()->get_str("serverName") << "\n";

        std::cout << ZincWriter::grid_to_string(*c.ops()) << "\n";

        std::cout << ZincWriter::grid_to_string(*c.formats()) << "\n";

        std::cout << c.read_by_id(Ref("A"))->to_string() << "\n";

        std::cout << c.read("site")->to_string() << "\n";

    }
    catch (std::exception& e)
    {
        std::cout << "Error: " << e.what() << "\n";
    }
}*/