#include "HTTPResponseStringParser.hpp"
#include <Poco/Net/HTTPResponse.h>
#include <Poco/NumberParser.h>

std::string HTTPResponseStringParser::parse( Poco::Net::HTTPResponse& response ) {

	// get header first
	response.read(_data);

	if (response.getStatus() < 200 || response.getStatus() == HTTPResponse::HTTP_NO_CONTENT || response.getStatus() == HTTPResponse::HTTP_NOT_MODIFIED)
		return getFixedLength(0);
	else if (response.getChunkedTransferEncoding())
		return getChunked();
	else if (response.hasContentLength())
		return getFixedLength(response.getContentLength());
	else {
		std::string res;
		_data >> res;
		return res;
	}
}

std::string HTTPResponseStringParser::getFixedLength(std::streamsize len) {
	std::string res(len, '\0');
	_data.read(&res[0], len);
	return res;
}

std::string HTTPResponseStringParser::getChunked() {
	std::string res = "";
	std::streamsize _chunk;

	static const int eof = std::char_traits<char>::eof();

	do {
		std::string chunkLen;

		int ch = _data.get();

		while (Poco::Ascii::isSpace(ch)) ch = _data.get();

		while (Poco::Ascii::isHexDigit(ch) && chunkLen.size() < 8) { chunkLen += (char) ch; ch = _data.get(); }

		if (ch != eof && !(Poco::Ascii::isSpace(ch) || ch == ';')) return res;
		while (ch != eof && ch != '\n') ch = _data.get();
		unsigned chunk;

		if (Poco::NumberParser::tryParseHex(chunkLen, chunk))
			_chunk = (std::streamsize) chunk;
		else
			break;

		if (_chunk > 0) {
			res.append(getFixedLength(_chunk));
		} else {
			int ch = _data.get();
			while (ch != eof && ch != '\n') ch = _data.get();
			return res;
		}
	} while (_chunk);
	
	return res;
}