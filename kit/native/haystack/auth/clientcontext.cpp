#include "clientcontext.hpp"
#include "authmsg.hpp"
#include "Poco/URI.h"
#include "Poco/Net/HTTPClientSession.h"
#include "scheme.hpp"
#include <algorithm>


using namespace haystack::Auth;
using namespace std;
using namespace Poco::Net;


AuthClientContext& AuthClientContext::open() {

	HTTPResponse helloResp;
	sendHello(helloResp);
	// first try standard authentication va RFC 7235 process
	if (openStd(helloResp)) return success();
	// check if we have a 200
	if (helloResp.getStatus() == HTTPResponse::HTTPStatus::HTTP_OK) return success();
	stringstream content;
	helloResp.write(content);

	if ( any_of( Scheme::registry.begin(), Scheme::registry.end(),
			[this, &helloResp, &content](Scheme::RegistryType::const_reference p) {
				return p.second->onClientNonStd(*this, helloResp, content.str());
			}
	)) {
		return success();
	}

	// give up
	HTTPResponse::HTTPStatus resCode = helloResp.getStatus();
	string resServer = helloResp["Server"];
	if (resCode / 100 >= 4) throw runtime_error("HTTP error code: " + resCode); // 4xx or 5xx

	stringstream expss;
	expss << "No suitable auth scheme for: " << static_cast<int>(resCode) << " " << resServer;
	throw runtime_error(expss.str());
}

void AuthClientContext::getHelloMsg(HTTPRequest& req) {

	HTTPAuthenticationParams authParams;
	//authParams.setRealm("hello");
	stringstream ss;
	Poco::Base64Encoder enc(ss, Poco::Base64EncodingOptions::BASE64_URL_ENCODING | Poco::Base64EncodingOptions::BASE64_NO_PADDING);
	enc << m_user;
	enc.close();
	authParams.add("username", ss.str());
	AuthMsg hello("hello", authParams);
	getAuthMsg(hello, req);
}

void AuthClientContext::sendHello(HTTPResponse& res) {
	Poco::URI uri(this->m_uri);
	HTTPClientSession session(uri.getHost(), uri.getPort());
	session.setTimeout(this->connectTimeout, this->readTimeout, this->readTimeout);

	HTTPAuthenticationParams authParams;
	//authParams.setRealm("hello");
	stringstream ss;
	Poco::Base64Encoder enc(ss, Poco::Base64EncodingOptions::BASE64_URL_ENCODING | Poco::Base64EncodingOptions::BASE64_NO_PADDING);
	enc << m_user;
	enc.close();
	authParams.add("username", ss.str());
	AuthMsg hello("hello", authParams);

	getAuth(hello, res);
}

void AuthClientContext::onSuccess( Scheme* scheme, HTTPResponse& resp ) {

	 // init the bearer token
    try {
    string authInfo =  resp.get("Authentication-Info");
   //AuthMsg authInfoMsg = AuthMsg.fromStr("bearer " + authInfo);

    // callback to scheme for client success
    scheme->onClientSuccess(*this, resp);

    // only keep authToken parameter for Authorization header
	HTTPAuthenticationParams params;
	params.fromAuthInfo(authInfo);


	NameValueCollection authParams;
	authParams.add("authToken", params["authToken"]);

    headers.add("Authorization", AuthMsg::encode("bearer", authParams));
    } catch(std::exception& ex){
	    std::cout<<__FILE__<<":"<<__LINE__<<":"<<ex.what()<<std::endl;
    }
}


bool AuthClientContext::asyncAuthStep(HTTPResponse& resp, HTTPRequest& req) {
	// 200 means we are done, 401 means keep looping,
	// consider anything else a failure

	if (resp.getStatus() == HTTPResponse::HTTPStatus::HTTP_OK) {
		// looks like we are done
		onSuccess(scheme, resp);
		success();
		return false;
	}
	// something wierd TBD
	if (resp.getStatus() != HTTPResponse::HTTPStatus::HTTP_UNAUTHORIZED) return false;
	
	string header = resp.get(HTTPAuthenticationParams::WWW_AUTHENTICATE);
	
	// don't use this mechanism for Basic which we
	// handle as a non-standard scheme because the headers
	// don't fit nicely into our restricted AuthMsg format


	if ( scheme == nullptr ) {
		string schemeName = header.substr( 0, header.find_first_of(' ') );

		std::transform(schemeName.begin(), schemeName.end(), schemeName.begin(), ::tolower );
		if (schemeName == "basic") return false;
		scheme = Scheme::find(schemeName);
	}
	
	HTTPAuthenticationParams authInfo;
	authInfo.fromAuthInfo(header.substr( header.find_first_of(' ') ));
	
	// let scheme handle message
	HTTPAuthenticationParams reqParams;
	scheme->onClient(*this, authInfo, reqParams);
	// send request back to the server
	AuthMsg reqMsg(scheme->name, reqParams);
	getAuthMsg(reqMsg, req);
	return true; // baby one more time
}

bool AuthClientContext::openStd(HTTPResponse& resp) {
	// must be 401 challenge with WWW-Authenticate header
	if (resp.getStatus() != HTTPResponse::HTTPStatus::HTTP_UNAUTHORIZED) return false;
	string wwwAuth = resp.get(HTTPAuthenticationParams::WWW_AUTHENTICATE);
	std::transform(wwwAuth.begin(), wwwAuth.end(), wwwAuth.begin(), ::tolower );
	// don't use this mechanism for Basic which we
	// handle as a non-standard scheme because the headers
	// don't fit nicely into our restricted AuthMsg format

	if (wwwAuth.compare(0, sizeof("basic"), "basic") == 0) return false;

	// process res/req messages until we have 200 or non-401 failure

	Scheme* scheme;

    for (int loopCount = 0; true; ++loopCount)
    {
		// sanity check that we don't loop too many times
		if (loopCount > 5) throw runtime_error("Loop count exceeded");
	  
		// parse the WWW-Auth header and use the first scheme
		string header = resp.get(HTTPAuthenticationParams::WWW_AUTHENTICATE);
		string schemeName = header.substr( 0, header.find_first_of(' ') );
		HTTPAuthenticationParams authInfo;
		authInfo.fromAuthInfo(header.substr( header.find_first_of(' ') ));

		//cout << "'" << schemeName << "' '" << authInfo.toString() << "'" << endl;

		scheme = Scheme::find(schemeName);

		// let scheme handle message
		HTTPAuthenticationParams reqParams;
		scheme->onClient(*this, authInfo, reqParams);

		// send request back to the server
		AuthMsg reqMsg(scheme->name, reqParams);
		getAuth(reqMsg, resp );
		
		// 200 means we are done, 401 means keep looping,
		// consider anything else a failure
		if (resp.getStatus() == HTTPResponse::HTTPStatus::HTTP_OK) break;
		if (resp.getStatus() == HTTPResponse::HTTPStatus::HTTP_UNAUTHORIZED) continue;
		stringstream ss;
		ss <<  resp.getStatus() << " " << resp.getReason();
		throw runtime_error(ss.str());
	}

	onSuccess(scheme, resp);
    // we did it!
    return true;
}

void AuthClientContext::getAuthMsg(const AuthMsg& authInfo, HTTPRequest& req) {
	req.setMethod("GET");
	req.setURI(this->m_uri);
	prepare(req);
	req.set("Authorization", authInfo.toString());
}

void AuthClientContext::getAuth(const AuthMsg& authInfo, HTTPResponse& outRes ) {
	Poco::URI uri(this->m_uri);
	HTTPClientSession session(uri.getHost(), uri.getPort());
	session.setTimeout(this->connectTimeout, this->readTimeout, this->readTimeout);
	HTTPRequest req;
	getAuthMsg(authInfo, req);
	
//	{
//		cout << ">>>>>>>>>>>>>> SENDING: >>>>>>>>>>>>>>>" << endl;
//		req.write(cout);
//		cout << " >>>>>>>>>>>>>>>>" << endl;
//	}

	session.sendRequest(req);
	session.receiveResponse(outRes);
//	{
//		cout << "<<<<<<<<<<<<<< RECEIVED: <<<<<<<<<<<<<<" << endl;
//		outRes.write(cout);
//		cout << " <<<<<<<<<<<<<<" << endl;
//	}
}
