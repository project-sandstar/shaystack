#pragma once
#include "scheme.hpp"

using namespace haystack::Auth;

class BasicScheme : public Scheme {
public:
    BasicScheme() : Scheme( "basic" ) {}

    void onClient(AuthClientContext& cx, const NameValueCollection& authInfo, NameValueCollection& outAuthParams) const override {
        throw std::runtime_error("not implemented");
    }

    void onClientSuccess(const AuthClientContext& cx, const NameValueCollection& authInfo) override;

    bool onClientNonStd(const AuthClientContext& cx, const HTTPResponse& resp, const string& content) override;

    ~BasicScheme() {}
};
