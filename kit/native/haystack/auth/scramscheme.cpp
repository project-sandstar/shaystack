#include "scramscheme.hpp"
#include "authmsg.hpp"
#include "Poco/RandomStream.h"
#include "Poco/Base64Decoder.h"
#include "Poco/Base64Encoder.h"
#include "Poco/PBKDF2Engine.h"
#include <iostream>
#include <string>
#include <openssl/evp.h>
#include "Poco/HMACEngine.h"
#include "Poco/DigestEngine.h"
#include <iomanip>

using namespace haystack::Auth;
using namespace Poco;
using std::stringstream;
using Digest = Poco::DigestEngine::Digest;

const string ScramScheme::GS2_HEADER = "n,,";

void ScramScheme::firstMsg( AuthClientContext& cx, const NameValueCollection& authInfo, NameValueCollection& outAuthParams) const {
	// construct client-first-message
    const string c_nonce = genNonce();
    const string c1_bare = "n=" + cx.m_user + ",r=" + c_nonce;
    string c1_msg  = ScramScheme::GS2_HEADER + c1_bare;

    // stash for final msg
    cx.stash.add("c_nonce", c_nonce);
    cx.stash.add("c1_bare", c1_bare);

    // build auth msg
	stringstream ss;
	Poco::Base64Encoder enc(ss, Poco::Base64EncodingOptions::BASE64_URL_ENCODING | Poco::Base64EncodingOptions::BASE64_NO_PADDING);
	enc << c1_msg;
	enc.close();
    outAuthParams.add("data", ss.str());
	injectHandshakeToken(authInfo, outAuthParams);
    //return AuthMsg(name, outAuthParams);
}

void ScramScheme::finalMsg( AuthClientContext& cx, const NameValueCollection& authInfo, NameValueCollection& outAuthParams) const {
	// decode server-first-message
	HTTPAuthenticationParams data;
	string s1_msg;
	{
		stringstream ss(authInfo["data"]);
		Base64Decoder dec(ss, Poco::Base64EncodingOptions::BASE64_URL_ENCODING | Poco::Base64EncodingOptions::BASE64_NO_PADDING);
		dec >> s1_msg;
//		cout << "final, s1_msg: " << s1_msg << endl;
		data.fromAuthInfo(s1_msg);
	}

    // c2-no-proof
    string channel_binding = Base64StandartEncode(ScramScheme::GS2_HEADER);
	
    string nonce           = data["r"];
    string c2_no_proof     = "c=" + channel_binding + ",r=" + nonce;

    // proof
    string hash = authInfo["hash"];
    string salt = data["s"];
    int iterations = stoi(data["i"]);
	
    string c1_bare = cx.stash.get("c1_bare");
    string authMsg = c1_bare + "," + s1_msg + "," + c2_no_proof;
//	cout << "final, authMsg: " << authMsg << endl;

    string c2_msg;
//    try
//    {
		using namespace Poco::Crypto;
		PBKDF2Engine<HMACEngine<SHA256Engine> > pbkdf2(Base64Decode(salt), iterations);
		pbkdf2.update(cx.m_pass);
		Digest saltedPassword = pbkdf2.digest();
		
		string clientProof = createClientProof(hash, saltedPassword, authMsg);
		c2_msg = c2_no_proof + ",p=" + clientProof;
//    }
//    catch (exception& e)
//    {
//      throw runtime_error("Failed to compute scram", e);
//    }

    // build auth msg
//		cout << "final msg, c2_msg: " << c2_msg << endl;
	stringstream ss;
	Base64Encoder enc(ss, Poco::Base64EncodingOptions::BASE64_URL_ENCODING | Poco::Base64EncodingOptions::BASE64_NO_PADDING);
	enc << c2_msg;
	enc.close();
    outAuthParams.add("data", ss.str());
    injectHandshakeToken(authInfo, outAuthParams);
}

void ScramScheme::onClient(AuthClientContext& cx, const NameValueCollection& authInfo, NameValueCollection& outAuthParams) const {
	authInfo.has("data") ? finalMsg(cx, authInfo, outAuthParams) : firstMsg(cx, authInfo, outAuthParams);
}

void ScramScheme::injectHandshakeToken(const NameValueCollection& msg, NameValueCollection& params) {
    if (msg.has("handshakeToken")) {
		params.add("handshakeToken", msg["handshakeToken"]);
	}
}

string ScramScheme::genNonce() const {
	Digest bytes(ScramScheme::CLIENT_NONCE_BYTES);
    Poco::RandomBuf random;
	random.readFromDevice(reinterpret_cast<char*>(bytes.data()), bytes.size());

	stringstream ss;
	ss << bytes;
    return ss.str();
}

Digest hmac(const Digest& key, const string& str) {
	Poco::HMACEngine<SHA256Engine> hmac(string(key.begin(), key.end()));
	hmac.update(str);
	return hmac.digest();
}

string ScramScheme::createClientProof(const string& hash, const Digest& saltedPassword, const string& authMsg) {
	if (hash != "SHA-256") throw runtime_error(hash);

 	Digest clientKey = hmac(saltedPassword, "Client Key");

	SHA256Engine sha256;
	sha256.update(clientKey.data(), clientKey.size());
	Digest storedKey = sha256.digest();

	Digest clientSig = hmac(storedKey, authMsg);

	Digest clientProof(clientKey.size());
	for ( int i = 0; i < clientKey.size(); i++ )
		clientProof[i] = clientKey[i] ^ clientSig[i];

	stringstream ss;
	Poco::Base64Encoder enc(ss);
	enc << string(clientProof.begin(), clientProof.end());
	enc.close();
	return ss.str();
}

std::ostream& operator <<(std::ostream& out, const Digest& dig) {
	out << hex ;
	for ( auto c : dig ) {
		out << setfill('0') << setw(2) << (unsigned int)c;
	}
	out << dec;
	return out;
}