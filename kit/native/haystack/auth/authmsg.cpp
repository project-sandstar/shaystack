#include <sstream>
#include "authmsg.hpp"
using namespace haystack::Auth;
using namespace std;

std::string  AuthMsg::encode(const string& scheme, const NameValueCollection& params) {

	stringstream result;

	result << scheme << " ";

	NameValueCollection::ConstIterator iter = params.begin();


	if (iter != params.end())
	{
			result << iter->first << "=" << iter->second;
			++iter;
	}

	for (; iter != params.end(); ++iter)
	{
			result << ", ";
			result << iter->first << "=" << iter->second;
	}

	return result.str();
}