#pragma once
#include <string>
#include "Poco/Net/NameValueCollection.h"
#include <algorithm>
using namespace std;
using namespace Poco::Net;

namespace haystack { namespace Auth {
/**
 * AuthMsg models a scheme name and set of parameters according
 * to <a href="https://tools.ietf.org/html/rfc7235">RFC 7235</a>. To simplify
 * parsing, we restrict the grammar to be auth-param and token (the
 * token68 and quoted-string productions are not allowed).
 */
class AuthMsg {
public:
    AuthMsg(const string& scheme_, const NameValueCollection& params_): m_params( params_ )
    {
        scheme.resize(scheme_.length());
        std::transform(scheme_.begin(), scheme_.end(), scheme.begin(), ::tolower);
        m_str = AuthMsg::encode(scheme, m_params);
    }

    const string& toString() const { return m_str; }

    static std::string encode(const string& scheme, const NameValueCollection& params);

    string scheme;
    NameValueCollection m_params;

private:

    string m_str;
};

}}