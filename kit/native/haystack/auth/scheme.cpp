#include "scheme.hpp"
#include "basicscheme.hpp"
#include "scramscheme.hpp"
#include <utility>
#include <memory>



using namespace std;
using namespace haystack::Auth;

Scheme::RegistryType initSchemes() {
	Scheme::RegistryType m;
	m.emplace("basic", unique_ptr< BasicScheme >(new BasicScheme()));
	m.emplace("scram", unique_ptr< ScramScheme >(new ScramScheme()));
	return m;
}

Scheme::RegistryType Scheme::registry = initSchemes();

Scheme* Scheme::find(string name, bool checked) {
	if (checked)
		return Scheme::registry.at(name).get();

	return Scheme::registry[name].get();
}
