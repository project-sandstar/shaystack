#pragma once
#include <string>
#include <sstream>
#include <iostream>
#include <locale>
#include <functional>
#include "Poco/Net/HTTPAuthenticationParams.h"
#include "Poco/Net/HTTPRequest.h"
#include "Poco/Net/HTTPResponse.h"
#include "Poco/Base64Encoder.h"
#include "authmsg.hpp"

using namespace std;
using namespace Poco::Net;



namespace haystack {
namespace Auth {
    class Scheme;
    class AuthClientContext {
    public:
        string m_uri; /** URI used to open the connection */
        string m_user; /** Username used to open the connection */
        string m_pass; /** Plaintext password for authentication */
        string m_userAgent = "HaystackSandstar"; /** User agent string */
        NameValueCollection headers; /** Headers we wish to use for authentication requests */

        /**
         * Stash allows you to store state between messages
         * while authenticating with the server.
         */
        NameValueCollection stash;

        int connectTimeout = 60 * 1000;
        int readTimeout = 60* 1000;

        AuthClientContext( const string& uri, const string& user, const string& pass ) {
            this->m_uri = uri;
            this->m_user = user;
            this->m_pass = pass;
        }

        bool isAuthenticated() { return m_authenticated; }
        bool clearAuth() { m_authenticated = false; }

        AuthClientContext& open();


        void getHelloMsg(HTTPRequest& req);
        void sendHello(HTTPResponse& res);


        void prepare(HTTPRequest& req) {

            for( auto &h : this->headers ) {
                req.set( h.first, h.second );
            }
            
            if ( m_userAgent.length() > 0 ) {
                req.set("User-Agent", this->m_userAgent);
            }
        }

        // returns string of http request
        void getAuthMsg(const AuthMsg& authInfo, HTTPRequest& req);

        /**
         *  async support for auth challenge step
         * @ return false if done, either success or not, need to check is_authenticated()
         */
        bool asyncAuthStep(HTTPResponse& resp, HTTPRequest& req);

    private:

        /**
         * Attempt standard authentication via Haystack/RFC 7235
         *
         * @param resp The response to the hello message
         * @return true if haystack authentciation was used, false if the
         * server does not appear to implement RFC 7235.
         */
        bool openStd(HTTPResponse& resp);

        inline AuthClientContext& success() {
            this->m_authenticated = true;
            return *this;
        }

        void onSuccess( Scheme*, HTTPResponse& outRes );

        void getAuth(const AuthMsg& authInfo, HTTPResponse& outRes );

        bool m_authenticated = false;

        Scheme* scheme = nullptr;


        
    };
}
}
