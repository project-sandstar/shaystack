#pragma once
#include <string>
#include <sstream>
#include <iterator>
#include "scheme.hpp"
#include "authmsg.hpp"
#include "Poco/Crypto/DigestEngine.h"
#include "Poco/Base64Decoder.h"
#include "Poco/StreamCopier.h"

using namespace std;
using namespace haystack::Auth;

class SHA256Engine : public Poco::Crypto::DigestEngine
{
public:
    enum
    {
        BLOCK_SIZE = 64,
        DIGEST_SIZE = 32
    };

    SHA256Engine() : DigestEngine("SHA256") {}
};

template< typename D >
inline string Base64StandartEncode(const D& data) {
    stringstream ss;
    Poco::Base64Encoder enc(ss);
    enc << data;
    enc.close();
    return ss.str();
}

template< typename D >
inline string Base64Decode(const D& data, int options = 0) {
    istringstream ss(data);
    std::string result;
    Poco::Base64Decoder dec(ss, options);
    Poco::StreamCopier::copyToString( dec, result );
    return result;
}

namespace haystack { namespace Auth {

/**
 * ScramScheme implements the salted challenge response authentication
 * mechanism as defined in <a href="https://tools.ietf.org/html/rfc5802">RFC 5802</a>
 */
class ScramScheme : public Scheme {
public:
    ScramScheme(): Scheme("scram") {}
    void onClient(AuthClientContext& cx, const NameValueCollection& authInfo, NameValueCollection& outAuthParams) const override;

    ~ScramScheme() {}

private:
    void firstMsg( AuthClientContext& cx, const NameValueCollection& msg, NameValueCollection& outReq) const;
    void finalMsg( AuthClientContext& cx, const NameValueCollection& msg, NameValueCollection& outReq) const;
    /** Generate a random nonce string */
    string genNonce() const;
    static string createClientProof(const string& hash, const Poco::DigestEngine::Digest& saltedPassword, const string& authMsg);

    /** If the msg contains a handshake token, inject it into the given params */
    static void injectHandshakeToken(const NameValueCollection& msg, NameValueCollection& params);
    static const int CLIENT_NONCE_BYTES = 16;
    static const string GS2_HEADER;
};

}}

std::ostream& operator <<(std::ostream& out, const Poco::DigestEngine::Digest& dig);