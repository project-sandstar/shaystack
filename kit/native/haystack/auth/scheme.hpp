#pragma once

#include "Poco/Net/HTTPRequest.h"
#include "clientcontext.hpp"
#include <memory>
#include <string>
#include <map>

using namespace std;
using namespace Poco::Net;

namespace haystack {
namespace Auth {
class Scheme {

public:

    typedef map< string, unique_ptr< Scheme > > RegistryType;
    static RegistryType registry;
    
    /**
     * Lookup an AuthScheme for the given case-insensitive name.
     */
    static Scheme* find(string name, bool checked = true);
    
    /**
     * Scheme name (always normalized to lowercase)
     */
    string name;

    /**
     * Handle a standardized client authentication challenge message from
     * the server using RFC 7235.
     *
     * @param cx the current {@link AuthClientContext}
     * @param msg the {@link HTTPResponse} sent by the server
     * @return The {@link AuthMsg} to send back to the server to authenticate
     */
    virtual void onClient(AuthClientContext& cx, const NameValueCollection& authInfo, NameValueCollection& outAuthParams) const {}

    /**
     * Callback after successful authentication with the server.
     * The default implementation is a no-op.
     *
     * @param cx the current {@link AuthClientContext}
     * @param msg the {@link AuthMsg} sent by the server when it authenticated
     *            the client.
     *
     */
    virtual void onClientSuccess(const AuthClientContext& cx, const NameValueCollection& authInfo)
    {
    }

    /**
     * Handle non-standardized client authentication when the standard
     * process (RFC 7235) fails. If this scheme thinks it can handle the
     * given response by sniffing the response code and headers, then it
     * should process and return true.
     *
     * @param cx the current {@link AuthClientContext}
     * @param resp the response message from the server
     * @param content the body of the response if it had one, or null.
     * @return true if the scheme processed the response, false otherwise. Returns false by default.
     */
    virtual bool onClientNonStd(const AuthClientContext& cx, const HTTPResponse& resp, const string& content)
    {
        return false;
    }

    virtual ~Scheme() {};

    protected:
    Scheme(const string& name_) : name( name_ )
    {
      //if (name != name.toLowerCase()) throw new IllegalArgumentException("Name must be lowercase: " + name);
      //this.name = name;
    }
private:
   

};
}
}
