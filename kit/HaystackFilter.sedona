class HaystackFilter extends HaystackPoint {
	@config property int clientId = -1
	@config @asStr property Buf(255) expression = ""
	@asStr property Buf(255) pointId = ""
	@asStr property Buf(255) dis = ""
	property bool query = true
	define Log log

	public override virtual void changed(Slot slot) {
		super.changed(slot)
		if (slot.name == "expression") {
			pointId.copyFromStr("")
			dis.copyFromStr("")
			changed(HaystackFilter.pointId)
			changed(HaystackFilter.dis)
			if ( !HaystackDevice.is_filter_valid(expression.toStr()) ) return
			setFault(false)
			doQuery()
		}

		if ( slot.name == "pointId" || slot.name == "dis" ) {
			copyToSlots(slot)
		}
    
    if (slot.name == "clientId") {
      // re-scan for whether new Haystack Device is added
      device = findDevice(clientId)
    }
	}

	public override virtual int linkEvent(int eType, Link link) {
		
		int res := super.linkEvent(eType, link)
		int slotId := this.id == link.fromComp ? link.fromSlot : link.toSlot
		Slot slot := this.type.slot(slotId)
		if ( slot.name == "pointId" || slot.name == "dis" ) {
			copyToSlots(slot)
		}

		return res
	}

	// copy bufs to slots
	private void copyToSlots(Slot src) {
		Buf srcBuf := this.getBuf(src)
		for ( Link l = linksFrom; l != null; l = l.nextFrom ) {
			if ( l.fromSlot != src.id ) continue
			Component dstComp := Sys.app.lookup(l.toComp)
			Slot dstSlot := dstComp.type.slot(l.toSlot)
			Buf dstBuf := dstComp.getBuf(dstSlot)
			dstBuf.copyFromBuf(srcBuf)
			dstComp.changed(dstSlot)
		}
	}

	action void doQuery() {
		query := true
	}

	public virtual override void execute() {

		super.execute()
		ticks = Sys.ticks();
		if ( waitingMessage ) return
		if ( !query ) return
		if ( !HaystackDevice.is_filter_valid(expression.toStr()) ) return

		if ( device == null ) device = findDevice(clientId)
		if ( device == null || !device.isAuthenticated() ) {
      setFault(true)
      return
    }

		HaystackDevice.read_message(device.client, expression.toStr(), reqbuf,  bufLen)
		if ( device.worker.write(reqbuf, this, HaystackFilter.onRead) ) {
			waitingMessage = true
			query := false
		}
	}

	action void onRead(Buf response) {
		waitingMessage = false

		if ( HaystackDevice.is_err(response.bytes) ) {
			setFault(true)
			return
		}

		setFault(false)

		if ( HaystackDevice.parse_str_response(response.bytes, 0, "id", false, parsedVal, parseValLen) ) {
			pointId.copyFromStr(parsedVal)
			changed(HaystackFilter.pointId)
		}

		if ( HaystackDevice.parse_str_response(response.bytes, 0, "dis", false, parsedVal, parseValLen) ) {
			dis.copyFromStr(parsedVal)
			changed(HaystackFilter.dis)
		} else {
			//do dis() query for id
			if ( device == null ) device = findDevice(clientId)
			if ( device == null ) {
        setFault(true)
        return
      }
      setFault(false)
      
			dis.clear()
			bufstream.print("readLink(@${pointId.toStr()}).dis()")
			HaystackDevice.eval_message(device.client, dis.toStr(), reqbuf, bufLen)
			if ( device.worker.write(reqbuf, this, HaystackFilter.onReadDis) ) {
				waitingMessage = true
			}
		}
	}

	action void onReadDis(Buf response) {
		waitingMessage = false

		if ( HaystackDevice.is_err(response.bytes) ) {
			setFault(true)
			return
		}

		setFault(false)

		if ( HaystackDevice.parse_str_response(response.bytes, 0, "val", false, parsedVal, parseValLen) ) {
			dis.copyFromStr(parsedVal)
			changed(HaystackFilter.dis)
		}
	}

	define int bufLen = 4096
	internal inline Str(bufLen) reqbuf
	
	define int parseValLen = 255
	inline Str(parseValLen) parsedVal
	internal inline BufOutStream(dis) bufstream
}