# shaystack

## develoment build

	git clone <url>
	git submodule init && git submodule update
	cd sandstar && git submodule init && git submodule update
	cd -
	mkdir build && cd build
	cmake -DCMAKE_C_FLAGS=-m32 -DCMAKE_CXX_FLAGS=-m32 -DCMAKE_SHARED_LINKER_FLAGS=-m32 -DOPENSSL_ROOT_DIR=/usr/lib/i386-linux-gnu/ ..
	make

## pocoproject 
for OpenSSL build update to 9778dacd1cfdf1ee6c95332212ae67cb030d0945 commit

add include to `Crypto/src/X509Certificate.cpp`:

	#include <openssl/bn.h>

## arm build
### prepare dev env
	sudo aptitude install git cmake gcc-arm-linux-gnueabihf g++-arm-linux-gnueabihf crossbuild-essential-armhf default-jdk
	
### install libssl for arm
add arch to apt:

	sudo dpkg --add-architecture armhf

add repo for armhf packages, for ubuntu bionic it would be:

	deb [arch=armhf] http://ports.ubuntu.com/ubuntu-ports bionic main
	deb [arch=armhf] http://ports.ubuntu.com/ubuntu-ports bionic-updates main

install libssl1.0-dev:armhf:

	sudo aptitude update && sudo aptitude install libssl1.0-dev:armhf

install boost library for amrhf:

	sudo apt-get install libboost-dev:armhf

create arm-build and invoke at it:

	cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE=$PWD/../sandstar/sandstar/platform/linux/armv7l.cmake -DOPENSSL_ROOT_DIR=/usr/lib/arm-linux-gnueabihf ..